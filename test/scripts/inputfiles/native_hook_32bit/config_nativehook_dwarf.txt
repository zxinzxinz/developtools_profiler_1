request_id: 1
session_config {
 buffers {
  pages: 16384
 }
}
plugin_configs {
 plugin_name: "nativehook"
 sample_interval: 5000
 config_data {
  save_file: false
  smb_pages: 16384
  max_stack_depth: 8
  process_name: "com.huawei.hmos.settings"
  string_compressed: true
  fp_unwind: false
  blocked: true
  callframe_compress: true
  record_accurately: true
  offline_symbolization: false
  statistics_interval: 10
  startup_mode: true
  js_stack_report: 1
  max_js_stack_depth: 2
 }
}